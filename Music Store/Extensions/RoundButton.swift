//
//  RoundButton.swift
//  Music Store
//
//  Created by Marty on 01/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

@IBDesignable
class RoundButton: UIButton {
    @IBInspectable var round: Bool = false {
        didSet {
            cornerRadius = 0
            self.layer.cornerRadius = round ? self.layer.bounds.height / 2 : 0
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
}
