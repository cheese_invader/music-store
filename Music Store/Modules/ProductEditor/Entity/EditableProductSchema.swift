//
//  EditableProductSchema.swift
//  Music Store
//
//  Created by Marty on 16/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit
import Foundation

class EditableProductSchema: Encodable, Decodable {
    var id         : Int?
    var title      : String?
    var description: String?
    var category   : String?
    var quantity   : Int?
    var image      : Data?
    var price      : Decimal?
    
    init(editableProduct: EditableProduct) {
        self.id = Int(editableProduct.id ?? "")
        self.title       = editableProduct.title
        self.description = editableProduct.description
        self.category    = editableProduct.category
        self.quantity    = editableProduct.count
        self.image       = editableProduct.image?.jpegData(compressionQuality: 0.5)
        self.price = editableProduct.price
    }
}

