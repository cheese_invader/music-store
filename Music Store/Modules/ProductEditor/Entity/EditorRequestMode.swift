//
//  EditorRequestMode.swift
//  Music Store
//
//  Created by Marty on 16/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

enum EditorRequestMode {
    case add
    case edit
}
