//
//  EditableProduct.swift
//  Music Store
//
//  Created by Marty on 16/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit
import Foundation

class EditableProduct {
    var id         : String?
    var title      : String?
    var description: String?
    var category   : String?
    var count      : Int?
    var image      : UIImage?
    var price      : Decimal?
}

