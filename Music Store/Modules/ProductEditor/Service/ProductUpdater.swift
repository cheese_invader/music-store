//
//  ProductUpdater.swift
//  Music Store
//
//  Created by Marty on 16/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//


import Foundation


class ProductUpdater {
    private var task: URLSessionDataTask?
    var completionSuccessHandler: (() -> Void)?
    var completionFailureHandler: ((String) -> Void)?
    
    deinit {
        task?.cancel()
    }
    
    private func composeWithRequest(_ request: URLRequest) {
        task = URLSession.shared.dataTask(with: request, completionHandler: { [weak self] (data, response, error) in
            guard let self = self else {
                return
            }
            
            if let error = error {
                self.completionFailureHandler?(error.localizedDescription)
                return
            }
            
            guard let urlResponse = response as? HTTPURLResponse else {
                self.completionFailureHandler?("Product updating failure")
                return
            }
            
            switch urlResponse.statusCode {
            case 200:
                self.completionSuccessHandler?()
            default:
                self.completionFailureHandler?("Not OK status")
            }
            return
        })
    }
    
    func perform(product: EditableProductSchema, mode: EditorRequestMode) {
        task?.cancel()
        
        var requestUrl = Config.shared.serverURL
        
        switch mode {
        case .add:
            requestUrl.appendPathComponent(Config.shared.api_AddProduct)
        case .edit:
            requestUrl.appendPathComponent(Config.shared.api_UpdateProduct)
        }
        
        var request = URLRequest(url: requestUrl)
        
        let jsonEncoder = JSONEncoder()
        
        guard let jsonData = try? jsonEncoder.encode(product) else {
            completionFailureHandler?("Json compose error")
            return
        }
        
        request.httpBody = jsonData
        
        request.httpMethod = "POST"
        request.setValue("Bearer \(UserInfo.shared.accessToken!)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        composeWithRequest(request)
        
        task?.resume()
    }
}

