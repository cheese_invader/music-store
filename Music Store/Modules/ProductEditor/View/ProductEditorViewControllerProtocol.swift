//
//  ProductEditorViewControllerProtocol.swift
//  Music Store
//
//  Created by Marty on 16/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol ProductEditorViewControllerProtocol: class {
    var product: EditableProduct { get }
    var isDoneButtonEnabled: Bool { get set }
}
