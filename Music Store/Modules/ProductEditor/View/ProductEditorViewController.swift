//
//  ProductEditorViewController.swift
//  Music Store
//
//  Created by Marty on 16/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit
import Foundation

class ProductEditorViewController: UIViewController, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ProductEditorViewControllerProtocol {
    // MARK: - VIPER -
    private var presenter: ProductEditorPresenterProtocol?
    
    func setupPresenter(_ presenter: ProductEditorPresenterProtocol) {
        self.presenter = presenter
    }
    
    private let imagePicker = UIImagePickerController()
    
    private var productId = ""
    private var isDescriptionEmpty = true
    private var blurEffect: UIVisualEffect?
    
    @IBOutlet weak var blurVisualEffect: UIVisualEffectView!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var countTextField: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    @IBAction func doneButtonWasTapped(_ sender: UIBarButtonItem) {
        presenter?.doneButtonWasTapped()
    }
    
    @IBAction func selectImageButtonWasTapped(_ sender: UIButton) {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        navigationController?.present(imagePicker, animated: true, completion: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.blurEffect = self.blurVisualEffect.effect
        self.blurVisualEffect.effect = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 1) { [weak self] in
            guard let self = self else {
                return
            }
            
            self.blurVisualEffect.effect = self.blurEffect
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        hideKeyboardWhenTappedAround()
        makePlaceholdersWhite()
    }
    
    private func makePlaceholdersWhite() {
        titleTextField.attributedPlaceholder = NSAttributedString(string: "title", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        categoryTextField.attributedPlaceholder = NSAttributedString(string: "category", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        priceTextField.attributedPlaceholder = NSAttributedString(string: "price", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        countTextField.attributedPlaceholder = NSAttributedString(string: "count", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])

        descriptionTextView.delegate = self
        descriptionTextView.text = "Description"
        descriptionTextView.textColor = UIColor.lightGray
    }
    
    // MARK: - UITextViewDelegate -
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            isDescriptionEmpty = true
            textView.text = nil
            textView.textColor = UIColor.white
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            isDescriptionEmpty = true
            textView.text = "Description"
            textView.textColor = UIColor.lightGray
        } else {
            isDescriptionEmpty = false
        }
    }
    
    
    // MARK: - ProductEditorViewControllerProtocol -
    
    var isDoneButtonEnabled: Bool {
        get {
            return doneButton.isEnabled
        }
        set {
            doneButton.isEnabled = newValue
        }
    }
    
    var product: EditableProduct {
        get {
            let editableProduct = EditableProduct()
            
            editableProduct.image = UIImage(named: "Kirk")!
            
            if let title = titleTextField.text, !title.isEmpty {
                editableProduct.title = title
            }
            
            if let category = categoryTextField.text, !category.isEmpty {
                editableProduct.category = category
            }
            if let count = Int(countTextField.text ?? "") {
                editableProduct.count = count
            }
            
            if let price = Decimal(string: (priceTextField.text ?? "").replacingOccurrences(of: ",", with: ".")) {
                editableProduct.price = price
            }
            
            if let description = descriptionTextView.text, !isDescriptionEmpty {
                editableProduct.description = description
            }
            
            editableProduct.image = image.image
            
            return editableProduct
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage {
            self.image.image = image
        }
        
        navigationController?.dismiss(animated: true, completion: nil)
    }
}
