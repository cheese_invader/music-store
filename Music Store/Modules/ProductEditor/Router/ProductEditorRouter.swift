//
//  ProductEditorRouter.swift
//  Music Store
//
//  Created by Marty on 16/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit


private let storyboardName = "ProductEditor"
private let storyboardID   = "ProductEditorStoryboard"


class ProductEditorRouter: ProductEditorRouterProtocol {
    // MARK: - VIPER -
    private weak var presenter:  ProductEditorPresenterProtocol?
    private weak var navigationController: NavigationViewController?
    
    init(presenter:  ProductEditorPresenterProtocol, navigationController: NavigationViewController) {
        self.navigationController = navigationController
        self.presenter            = presenter
    }
    
    
    static func assembleMudule(embededIn navigationController: NavigationViewController, productId: String, mode: EditorRequestMode) -> ProductEditorViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let view = storyboard.instantiateViewController(withIdentifier: storyboardID) as! ProductEditorViewController
        
        let presenter = ProductEditorPresenter()
        let interactor = ProductEditorInteractor(presenter: presenter, productId: productId, mode: mode)
        let router = ProductEditorRouter(presenter: presenter, navigationController: navigationController)
        
        presenter.setup(view: view, router: router, interactor: interactor)
        view.setupPresenter(presenter)
        
        return view
    }
    
    func showUpdateFailureAlert() {
        guard let navigationController = navigationController else {
            return
        }
        navigationController.showQuickAllert(title: "Updating error", message: "Try it later")
    }
    
    func dismiss() {
        guard let navigationController = navigationController else {
            return
        }
        
        navigationController.popToRootViewController(animated: true)
    }
}
