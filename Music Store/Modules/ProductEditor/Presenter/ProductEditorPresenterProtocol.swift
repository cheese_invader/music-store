//
//  ProductEditorPresenterProtocol.swift
//  Music Store
//
//  Created by Marty on 16/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol ProductEditorPresenterProtocol: class {
    func lockSending()
    func unlockSending()
    func doneButtonWasTapped()
    func updatingFailed(reason: String)
    func updatingSucceed()
}
