//
//  ProductEditorPresenter.swift
//  Music Store
//
//  Created by Marty on 16/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class ProductEditorPresenter: ProductEditorPresenterProtocol {
    // MARK: - VIPER -
    private weak var view : ProductEditorViewControllerProtocol?
    private var router    : ProductEditorRouterProtocol?
    private var interactor: ProductEditorInteractorProtocol?
    
    func setup(view: ProductEditorViewControllerProtocol, router: ProductEditorRouterProtocol, interactor: ProductEditorInteractorProtocol) {
        self.view       = view
        self.router     = router
        self.interactor = interactor
    }
    
    
    // MARK: - ProductEditorPresenterProtocol -
    
    func lockSending() {
        view?.isDoneButtonEnabled = false
    }
    
    func unlockSending() {
        view?.isDoneButtonEnabled = true
    }
    
    func doneButtonWasTapped() {
        guard let product = view?.product else {
            return
        }
        
        interactor?.userWantToEditProduct(product)
    }
    
    func updatingSucceed() {
        router?.dismiss()
    }
    
    func updatingFailed(reason: String) {
        router?.showUpdateFailureAlert()
    }
}
