//
//  ProductEditorInteractor.swift
//  Music Store
//
//  Created by Marty on 16/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class ProductEditorInteractor: ProductEditorInteractorProtocol {
    // MARK: - VIPER -
    private weak var presenter: ProductEditorPresenterProtocol?
    
    init(presenter: ProductEditorPresenterProtocol, productId: String, mode: EditorRequestMode) {
        self.presenter = presenter
        self.productId = productId
        self.requestMode = mode
        setupHandlers()
    }
    
    private let productId  : String
    private var requestMode: EditorRequestMode
    private var productUpdater = ProductUpdater()
    
    
    private func setupHandlers() {
        productUpdater.completionFailureHandler = { [weak self] (reason) in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.presenter?.unlockSending()
                self.presenter?.updatingFailed(reason: reason)
            }
        }
        
        productUpdater.completionSuccessHandler = { [weak self] in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.presenter?.unlockSending()
                self.presenter?.updatingSucceed()
            }
        }
    }
    
    // MARK: - ProductEditorInteractorProtocol -
    func userWantToEditProduct(_ product: EditableProduct) {
        product.id = requestMode == .edit ? productId : nil
        presenter?.lockSending()
        productUpdater.perform(product: EditableProductSchema(editableProduct: product), mode: requestMode)
    }
    
    func switchRequestModeTo(_ mode: EditorRequestMode) {
        self.requestMode = mode
    }
}
