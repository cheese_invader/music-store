//
//  ProductEditorInteractorProtocol.swift
//  Music Store
//
//  Created by Marty on 16/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol ProductEditorInteractorProtocol: class {
    func userWantToEditProduct(_ product: EditableProduct)
    func switchRequestModeTo(_ mode: EditorRequestMode)
}
