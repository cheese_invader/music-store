//
//  BasketInteractor.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class BasketInteractor: BasketInteractorProtocol {
    // MARK: - VIPER -
    private weak var presenter: BasketPresenterProtocol?
    
    init(presenter: BasketPresenterProtocol) {
        self.presenter = presenter
        setupHandlers()
    }
    
    private let buyRequest = BuyRequest()
    
    // MARK: - BasketInteractorProtocol -
    
    func order() {
        buyRequest.perform()
    }
    
    func setupHandlers() {
        buyRequest.completionSuccessHandler = { [weak self] in
            DispatchQueue.main.async {
                guard let self = self else {
                    return
                }
                Basket.shared.clear()
                self.presenter?.buyingSuccess()
            }
        }
        
        buyRequest.completionFailureHandler = { [weak self] (reason) in
            DispatchQueue.main.async {
                guard let self = self else {
                    return
                }
                self.presenter?.buyingFailured(reason)
            }
        }
    }
}
