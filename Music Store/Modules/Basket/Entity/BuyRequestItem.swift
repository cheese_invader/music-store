//
//  BuyRequestItem.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class BuyRequestItem: Encodable, Decodable {
    init(id: String, quantity: Int, pricePerItem: Decimal) {
        self.id = id
        self.quantity = quantity
        self.pricePerItem = pricePerItem
    }
    
    var id: String
    var quantity: Int
    var pricePerItem: Decimal
}
