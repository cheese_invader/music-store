//
//  Basket.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class Basket {
    static let shared = Basket()
    
    private init() {}
    
    public private(set) var elementsById = [String: ProductInBasket]()
    
    func addProduct(_ product: ProductInBasket, withId id: String) {
        if let prod = elementsById[id] {
            prod.count += product.count
        } else {
            elementsById[id] = product
        }
    }
    
    func removeProductById(_ id: String) {
        elementsById[id] = nil
    }
    
    func clear() {
        elementsById = [:]
    }
}
