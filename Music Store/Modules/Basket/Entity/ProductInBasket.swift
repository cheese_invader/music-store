//
//  ProductInBasket.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class ProductInBasket {
    init(name: String, count: Int, price: Decimal) {
        self.name = name
        self.count = count
        self.price = price
    }
    
    var name: String
    var count: Int
    var price: Decimal
}
