//
//  BuyRequestBasket.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class BuyRequestBasket: Encodable, Decodable {
    init() {
        for id in Basket.shared.elementsById.keys {
            let prod = Basket.shared.elementsById[id]!
            let requestProduct = BuyRequestItem(id: id, quantity: prod.count, pricePerItem: prod.price)
            productsToBuy.append(requestProduct)
            
        }
    }
    
    var productsToBuy = [BuyRequestItem]()
}
