//
//  BasketUpdaterProtocol.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol BasketUpdaterProtocol: class {
    func updateBasket()
    func order()
}
