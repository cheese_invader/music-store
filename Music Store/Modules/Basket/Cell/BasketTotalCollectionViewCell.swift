//
//  BasketTotalCollectionViewCell.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

class BasketTotalCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var confirmButton: UIButton!
    
    private weak var delegate: BasketUpdaterProtocol?

    func setup(total: Decimal) {
        totalLabel.text = "$\(total)"
        if total == 0 {
            confirmButton.isEnabled = false
        }
    }
    
    func setupDelegate(_ delegate: BasketUpdaterProtocol) {
        self.delegate = delegate
    }
    
    @IBAction func confirmButtonWasTapped(_ sender: UIButton) {
        delegate?.order()
    }
}
