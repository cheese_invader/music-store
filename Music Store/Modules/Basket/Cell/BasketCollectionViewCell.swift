//
//  BasketCollectionViewCell.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

class BasketCollectionViewCell: UICollectionViewCell {
    private var id = ""
    
    private weak var delegate: BasketUpdaterProtocol?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    
    func assembleWithProduct(_ product: ProductInBasket, withId id: String) {
        nameLabel.text = product.name
        priceLabel.text = "$\(product.price)"
        countLabel.text = "Count: \(product.count)"
        self.id = id
    }
    
    func setupDelegate(_ delegate: BasketUpdaterProtocol) {
        self.delegate = delegate
    }

    @IBAction func removeButtonWasTapped(_ sender: UIButton) {
        Basket.shared.removeProductById(id)
        delegate?.updateBasket()
    }
}
