//
//  BuyRequest.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//


import Foundation


class BuyRequest {
    private var task: URLSessionDataTask?
    var completionSuccessHandler: (() -> Void)?
    var completionFailureHandler: ((String) -> Void)?
    
    deinit {
        task?.cancel()
    }
    
    private func composeWithRequest(_ request: URLRequest) {
        task = URLSession.shared.dataTask(with: request, completionHandler: { [weak self] (data, response, error) in
            guard let self = self else {
                return
            }
            
            if let error = error {
                self.completionFailureHandler?(error.localizedDescription)
                return
            }
            
            guard let urlResponse = response as? HTTPURLResponse, let data = data else {
                self.completionFailureHandler?("Buying failure")
                return
            }
            
            switch urlResponse.statusCode {
            case 200:
                guard let buyResponse = try? JSONDecoder().decode(BuyResponse.self, from: data) else {
                    self.completionFailureHandler?("Wrong response format")
                    return
                }
                if buyResponse.isSuccess {
                    self.completionSuccessHandler?()
                } else {
                    self.completionFailureHandler?(buyResponse.message ?? "")
                }
            default:
                self.completionFailureHandler?("Not OK status")
            }
            return
        })
    }
    
    func perform() {
        task?.cancel()
        
        var request = URLRequest(url: Config.shared.serverURL.appendingPathComponent(Config.shared.api_Buy))
        request.httpMethod = "POST"
        request.setValue("Bearer \(UserInfo.shared.accessToken!)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        guard let jsonData = try? JSONEncoder().encode(BuyRequestBasket()) else {
            self.completionFailureHandler?("Json composing error")
            return
        }
        request.httpBody = jsonData
        composeWithRequest(request)
        
        task?.resume()
    }
}

