//
//  BasketPresenter.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class BasketPresenter: BasketPresenterProtocol {
    // MARK: - VIPER -
    private weak var view : BasketViewProtocol?
    private var router    : BasketRouterProtocol?
    private var interactor: BasketInteractorProtocol?
    
    func setup(view: BasketViewProtocol, router: BasketRouterProtocol, interactor: BasketInteractorProtocol) {
        
        self.view       = view
        self.router     = router
        self.interactor = interactor
    }
    
    // MARK: - BasketPresenterProtocol -
    func orderButtonWasTapped() {
        interactor?.order()
    }
    
    func buyingSuccess() {
        router?.showAlert(title: "Buying succeed", message: "Thnk you for your order")
        router?.dismiss()
    }
    
    func buyingFailured(_ reason: String) {
        router?.showAlert(title: "Buying failured", message: reason)
    }
}
