//
//  BasketPresenterProtocol.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol BasketPresenterProtocol: class {
    func orderButtonWasTapped()
    func buyingSuccess()
    func buyingFailured(_ reason: String)
}
