//
//  BasketRouter.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//


import UIKit


private let storyboardName = "Basket"
private let storyboardID   = "BasketStoryboard"


class BasketRouter: BasketRouterProtocol {
    // MARK: - VIPER -
    
    private weak var presenter: BasketPresenterProtocol?
    private weak var navigationController: NavigationViewController?
    
    init(presenter: BasketPresenterProtocol, navigationController: NavigationViewController) {
        self.navigationController = navigationController
        self.presenter            = presenter
    }
    
    static func assembleMudule(embededIn navigationController: NavigationViewController) -> BasketCollectionViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let view = storyboard.instantiateViewController(withIdentifier: storyboardID) as! BasketCollectionViewController
        
        let presenter = BasketPresenter()
        let interactor = BasketInteractor(presenter: presenter)
        let router = BasketRouter(presenter: presenter, navigationController: navigationController)
        
        presenter.setup(view: view, router: router, interactor: interactor)
        view.setupPresenter(presenter)
        
        return view
    }
    
    
    func showAlert(title: String, message: String) {
        guard let navigationController = navigationController else {
            return
        }
        navigationController.showQuickAllert(title: title, message: message)
    }
    
    func dismiss() {
        guard let navigationController = navigationController else {
            return
        }
        navigationController.popViewController(animated: true)
    }
}
