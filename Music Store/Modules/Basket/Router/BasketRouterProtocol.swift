//
//  BasketRouterProtocol.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol BasketRouterProtocol: class {
    func showAlert(title: String, message: String)
    func dismiss()
}
