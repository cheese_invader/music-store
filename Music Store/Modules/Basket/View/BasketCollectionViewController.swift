//
//  BasketCollectionViewController.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit


private let reuseIdentifier = "BasketCollectionViewCell"
private let reuseTotalIdentifier = "BasketTotalCollectionViewCell"

private let cellInset : CGFloat = 10


class BasketCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, BasketUpdaterProtocol, BasketViewProtocol {
    
    // MARK: - BasketUpdaterProtocol -
    func updateBasket() {
        productIds = Array(Basket.shared.elementsById.keys)
        collectionView.reloadData()
    }
    
    func order() {
        presenter?.orderButtonWasTapped()
    }
    
    
    // MARK: - VIPER -
    private var presenter: BasketPresenterProtocol?
    
    func setupPresenter(_ presenter: BasketPresenterProtocol) {
        self.presenter = presenter
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBackgroundImage()
        registerNib()
    }
    
    var productIds = Array(Basket.shared.elementsById.keys)
    
    
    // MARK: - Helpers -
    
    private func setBackgroundImage() {
        let bgImage = UIImage(named: "Les Paul")!
        let bgImageView = UIImageView(image: bgImage)
        bgImageView.contentMode = .scaleAspectFill
        collectionView.backgroundView = bgImageView
    }
    
    private func registerNib() {
        let nibCell = UINib(nibName: reuseIdentifier, bundle: nil)
        collectionView?.register(nibCell, forCellWithReuseIdentifier: reuseIdentifier)
        
        let nibTotalCell = UINib(nibName: reuseTotalIdentifier, bundle: nil)
        collectionView?.register(nibTotalCell, forCellWithReuseIdentifier: reuseTotalIdentifier)
    }
    

    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productIds.count + 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == productIds.count {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseTotalIdentifier, for: indexPath) as! BasketTotalCollectionViewCell
            
            var total: Decimal = 0
            for prod in Basket.shared.elementsById.values {
                total += (prod.price * Decimal(prod.count))
            }
            
            cell.setup(total: total)
            cell.setupDelegate(self)
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! BasketCollectionViewCell
        
        let id = productIds[indexPath.row]
        cell.assembleWithProduct(Basket.shared.elementsById[id]!, withId: id)
        cell.setupDelegate(self)
        
        return cell
    }
    
    
    // MARK: - UICollectionViewDelegateFlowLayout -
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: CGFloat(cellInset), left: CGFloat(cellInset),
                            bottom: CGFloat(cellInset), right: CGFloat(cellInset))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = UIScreen.main.bounds.width - 2 * cellInset
        let cellHeight = cellWidth / 3
        
        return CGSize(width: cellWidth, height: cellHeight)
    }
}
