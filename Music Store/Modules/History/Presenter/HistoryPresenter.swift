//
//  HistoryPresenter.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class HistoryPresenter: HistoryPresenterProtocol {
    // MARK: - VIPER -
    private weak var view : HistoryViewProtocol?
    private var router    : HistoryRouterProtocol?
    private var interactor: HistoryInteractorProtocol?
    
    func setup(view: HistoryViewProtocol, router: HistoryRouterProtocol, interactor: HistoryInteractorProtocol) {
        self.view       = view
        self.router     = router
        self.interactor = interactor
    }
    
    
    func readyToGetData() {
        interactor?.orderHistory()
    }
    
    func displayHistory(_ history: HistorySchema) {
        view?.setHistory(history)
    }
    
    func historyLoadingError() {
        router?.showAlert(title: "Error", message: "History loading error")
    }
}
