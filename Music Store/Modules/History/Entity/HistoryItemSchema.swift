//
//  HistoryItemSchema.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

struct HistoryItemSchema: Encodable, Decodable {
    var productName: String
    var quantity: Int
    var pricePerItem: Decimal
}
