//
//  HistorySchema.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

struct HistorySchema: Encodable, Decodable {
    var orders: [HistoryOrderSchema]
}
