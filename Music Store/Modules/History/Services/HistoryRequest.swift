//
//  HistoryRequest.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//


import Foundation


class HistoryRequest {
    private var task: URLSessionDataTask?
    var completionSuccessHandler: ((HistorySchema) -> Void)?
    var completionFailureHandler: ((String) -> Void)?
    
    deinit {
        task?.cancel()
    }
    
    private func composeWithRequest(_ request: URLRequest) {
        task = URLSession.shared.dataTask(with: request, completionHandler: { [weak self] (data, response, error) in
            guard let self = self else {
                return
            }
            
            if let error = error {
                self.completionFailureHandler?(error.localizedDescription)
                return
            }
            
            guard let data = data, let urlResponse = response as? HTTPURLResponse else {
                self.completionFailureHandler?("History request failure")
                return
            }
            
            switch urlResponse.statusCode {
            case 200:
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                guard let historySchema = try? decoder.decode(HistorySchema.self, from: data) else {
                    self.completionFailureHandler?("Wrong response format")
                    return
                }
                self.completionSuccessHandler?(historySchema)
            default:
                self.completionFailureHandler?("Not OK status")
            }
            return
        })
    }
    
    func perform() {
        task?.cancel()
        
        var requestUrl = Config.shared.serverURL
        requestUrl.appendPathComponent(Config.shared.api_History)
        
        var request = URLRequest(url: requestUrl)
        
        request.httpMethod = "GET"
        request.setValue("Bearer \(UserInfo.shared.accessToken!)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        composeWithRequest(request)
        
        task?.resume()
    }
}
