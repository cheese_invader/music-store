//
//  HistoryCollectionViewController.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit


private let reuseIdentifier = "HistoryCollectionViewCell"
private let reuseHeaderIdentifier = "SectionHeaderCollectionViewCell"

private let cellInset : CGFloat = 10


class HistoryCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, HistoryViewProtocol {

    // MARK: - VIPER -
    private var presenter: HistoryPresenterProtocol?
    
    func setupPresenter(_ presenter: HistoryPresenterProtocol) {
        self.presenter = presenter
    }
    
    
    private var history: HistorySchema?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setBackgroundImage()
        registerNib()
        presenter?.readyToGetData()
    }
    
    // MARK: - Helpers -
    private func setBackgroundImage() {
        let bgImage = UIImage(named: "Les Paul")!
        let bgImageView = UIImageView(image: bgImage)
        bgImageView.contentMode = .scaleAspectFill
        collectionView.backgroundView = bgImageView
    }
    
    private func registerNib() {
        let nibCell = UINib(nibName: reuseIdentifier, bundle: nil)
        collectionView?.register(nibCell, forCellWithReuseIdentifier: reuseIdentifier)
        
        let nibHeaderCell = UINib(nibName: reuseHeaderIdentifier, bundle: nil)
        collectionView?.register(nibHeaderCell, forCellWithReuseIdentifier: reuseHeaderIdentifier)
    }

    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        guard let history = history else {
            return 1
        }
        return history.orders.count
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let history = history else {
            return 0
        }
        return history.orders[section].items.count + 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseHeaderIdentifier, for: indexPath) as! SectionHeaderCollectionViewCell
            
            guard let history = history else {
                return cell
            }
            
            let dateInString = history.orders[indexPath.section].occuredOnUtc
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd' 'HH:mm:ss'Z'"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            let rawDate = dateFormatter.date(from: dateInString)
            
            guard let date = rawDate else {
                return cell
            }
            
            cell.setupWithDate(date)
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! HistoryCollectionViewCell
            
            guard let history = history else {
                return cell
            }
            
        cell.setupWithHistoryItemSchema(history.orders[indexPath.section].items[indexPath.row - 1])
            return cell
        }
    }
    
    
    // MARK: - UICollectionViewDelegateFlowLayout -
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: CGFloat(cellInset), left: CGFloat(cellInset),
                            bottom: CGFloat(cellInset), right: CGFloat(cellInset))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let cellWidth = UIScreen.main.bounds.width - cellInset * 2
        let cellHeight = cellWidth / 3

        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    
    func setHistory(_ history: HistorySchema) {
        self.history = history
        collectionView.reloadData()
    }
}
