//
//  HistoryRouter.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit


private let storyboardName = "History"
private let storyboardID   = "HistoryStoryboard"


class HistoryRouter: HistoryRouterProtocol {
    // MARK: - VIPER -
    private weak var presenter:  HistoryPresenterProtocol?
    private weak var navigationController: NavigationViewController?
    
    init(presenter:  HistoryPresenterProtocol, navigationController: NavigationViewController) {
        self.navigationController = navigationController
        self.presenter            = presenter
    }
    
    
    static func assembleMudule(embededIn navigationController: NavigationViewController) -> HistoryCollectionViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let view = storyboard.instantiateViewController(withIdentifier: storyboardID) as! HistoryCollectionViewController
        
        let presenter = HistoryPresenter()
        let interactor = HistoryInteractor(presenter: presenter)
        let router = HistoryRouter(presenter: presenter, navigationController: navigationController)
        
        presenter.setup(view: view, router: router, interactor: interactor)
        view.setupPresenter(presenter)
        
        return view
    }
    
    func showAlert(title: String, message: String) {
        guard let navigationController = navigationController else {
            return
        }
        navigationController.showQuickAllert(title: title, message: message)
    }
}
