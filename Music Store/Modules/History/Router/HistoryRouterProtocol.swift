//
//  HistoryRouterProtocol.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol HistoryRouterProtocol: class {
    func showAlert(title: String, message: String)
}
