//
//  HistoryInteractor.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class HistoryInteractor: HistoryInteractorProtocol {
    // MARK: - VIPER -
    private weak var presenter: HistoryPresenterProtocol?
    
    private let historyRequest = HistoryRequest()
    
    init(presenter: HistoryPresenterProtocol) {
        self.presenter = presenter
        setupHandlers()
    }
    
    func orderHistory() {
        historyRequest.perform()
    }
    
    func setupHandlers() {
        historyRequest.completionSuccessHandler = { [weak self] (history) in
            DispatchQueue.main.async {
                guard let self = self else {
                    return
                }
                self.presenter?.displayHistory(history)
            }
        }
        
        historyRequest.completionFailureHandler = { [weak self] (_) in
            DispatchQueue.main.async {
                guard let self = self else {
                    return
                }
                self.presenter?.historyLoadingError()
            }
        }
    }
}
