//
//  HistoryCollectionViewCell.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

class HistoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    func setupWithHistoryItemSchema(_ schema: HistoryItemSchema) {
        nameLabel.text = schema.productName
        countLabel.text = "Count: \(schema.quantity)"
        priceLabel.text = "$\(schema.pricePerItem)"
    }
}
