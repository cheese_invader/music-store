//
//  SectionHeaderCollectionViewCell.swift
//  Music Store
//
//  Created by Marty on 20/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

class SectionHeaderCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var dateLabel: UILabel!
    
    func setupWithDate(_ date: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, h:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        
        let timeStamp = dateFormatter.string(from: date)
        
        dateLabel.text = "\(timeStamp)"
    }
}
