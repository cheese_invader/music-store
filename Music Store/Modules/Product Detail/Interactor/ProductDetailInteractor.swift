//
//  ProductDetailInteractor.swift
//  Music Store
//
//  Created by Marty on 01/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class ProductDetailInteractor: ProductDetailInteractorProtocol {
    // MARK: - VIPER -
    private weak var presenter: ProductDetailPresenterProtocol?
    
    init(presenter: ProductDetailPresenterProtocol, product: Product) {
        self.presenter = presenter
        self.product = product
    }
    
    
    public let product: Product
    
    // MARK: - ProductDetailInteractorProtocol -
    
    var userRole: UserRole? {
        get {
            return UserInfo.shared.userRole
        }
    }
    
    func buy(_ count: Int) {
        Basket.shared.addProduct(ProductInBasket(name: product.title, count: count, price: product.price), withId: product.id)
        presenter?.productWasAddedToBasket()
    }
}
