//
//  ProductDetailInteractorProtocol.swift
//  Music Store
//
//  Created by Marty on 01/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol ProductDetailInteractorProtocol: class {
    var userRole: UserRole? { get }
    var product: Product { get }
    func buy(_ count: Int)
}
