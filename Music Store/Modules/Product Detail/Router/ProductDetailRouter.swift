//
//  ProductDetailRouter.swift
//  Music Store
//
//  Created by Marty on 01/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit


private let storyboardName = "ProductDetail"
private let storyboardID   = "ProductDetailStoryboard"


class ProductDetailRouter: ProductDetailRouterProtocol {
    // MARK: - VIPER -
    
    private weak var presenter:  ProductDetailPresenterProtocol?
    private weak var navigationController: NavigationViewController?
    
    init(presenter:  ProductDetailPresenterProtocol, navigationController: NavigationViewController) {
        self.navigationController = navigationController
        self.presenter            = presenter
    }
    
    
    static func assembleMudule(withProduct product: Product, embededIn navigationController: NavigationViewController) -> ProductDetailViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let view = storyboard.instantiateViewController(withIdentifier: storyboardID) as! ProductDetailViewController
        
        let presenter = ProductDetailPresenter()
        let interactor = ProductDetailInteractor(presenter: presenter, product: product)
        let router = ProductDetailRouter(presenter: presenter, navigationController: navigationController)
        
        presenter.setup(view: view, router: router, interactor: interactor)
        view.setupPresenter(presenter)
        
        return view
    }
    
    
    // MARK: - ProductDetailRouterProtocol -
    
    func showLoginView() {
        guard let navigationController = navigationController else {
            return
        }
        
        let loginView = LoginRouter.assembleMudule(embededIn: navigationController)
        navigationController.pushViewController(loginView, animated: true)
    }
    
    func showProductWasAddedToBasketAllert() {
        guard let navigationController = navigationController else {
            return
        }
        navigationController.showQuickAllert(title: "Added", message: "Open basket to order")
    }
    
    func showEditView(withProductId productId: String) {
        guard let navigationController = navigationController else {
            return
        }
        
        let editView = ProductEditorRouter.assembleMudule(embededIn: navigationController, productId: productId, mode: .edit)
        navigationController.pushViewController(editView, animated: true)
    }
}
