//
//  ProductDetailViewController.swift
//  Music Store
//
//  Created by Marty on 01/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

class ProductDetailViewController: UIViewController, ProductDetailViewControllerProtocol {
    // MARK: - VIPER -
    private var presenter: ProductDetailPresenterProtocol?
    
    func setupPresenter(_ presenter: ProductDetailPresenterProtocol) {
        self.presenter = presenter
    }
    
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var needCountStepper: UIStepper!
    @IBOutlet weak var needCountLabel: UILabel!
    @IBOutlet weak var descriptionTextField: UITextView!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var editButton: UIButton!
    
    
    @IBOutlet weak var blurVisualEffectView: UIVisualEffectView!
    
    
    @IBAction func buyButtonWasTapped(_ sender: UIButton) {
        presenter?.buyButtonWasTapped(withCount: Int(needCountStepper.value))
    }
    
    @IBAction func editButtonWasTapped(_ sender: UIButton) {
        presenter?.editButtonWasTapped()
    }
    
    @IBAction func needCountStepperValueWasChanged(_ sender: UIStepper) {
        needCount = Int(sender.value)
        needCountLabel.text = String(needCount)
    }
        
    private var needCount = 1
    private var blurEffect: UIVisualEffect?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter?.readyToDisplay()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.blurEffect = self.blurVisualEffectView.effect
        self.blurVisualEffectView.effect = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        UIView.animate(withDuration: 1) { [weak self] in
            guard let self = self else {
                return
            }

            self.blurVisualEffectView.effect = self.blurEffect
        }
    }
    
    
    // MARK: - ProductDetailViewControllerProtocol -
    
    var isEditButtonEnabled: Bool {
        get {
            return editButton.isEnabled
        }
        set {
            editButton.isEnabled = newValue
            editButton.isHidden  = !newValue
        }
    }
    
    func displayProduct(_ product: Product) {
        title = product.title
        productImage.image = product.image
        categoryLabel .text           = product.category
        countLabel    .text           = String(product.count)
        needCountLabel.text           = String(needCount)
        descriptionTextField.text     = product.description
        priceLabel.text               = "$\(product.price)"
        guard product.count > 0 else {
            needCountStepper.minimumValue = 0
            needCountStepper.maximumValue = 0
            needCountStepper.value        = 0
            return
        }
        
        needCountStepper.minimumValue = 1
        needCountStepper.maximumValue = Double(product.count)
        needCountStepper.value        = Double(needCount)
    }
}
