//
//  ProductDetailViewControllerProtocol.swift
//  Music Store
//
//  Created by Marty on 01/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol ProductDetailViewControllerProtocol: class {
    func displayProduct(_ product: Product)
    var isEditButtonEnabled: Bool { get set }
}
