//
//  ProductDetailPresenterProtocol.swift
//  Music Store
//
//  Created by Marty on 01/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol ProductDetailPresenterProtocol: class {
    // View ->
    func readyToDisplay()
    func editButtonWasTapped()
    func buyButtonWasTapped(withCount count: Int)
    
    // Interactor ->
    func setupViewForUserRole(_ userRold: UserRole?)
    func productWasAddedToBasket()
}
