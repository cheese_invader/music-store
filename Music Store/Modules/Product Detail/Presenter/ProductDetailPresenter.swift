//
//  ProductDetailPresenter.swift
//  Music Store
//
//  Created by Marty on 01/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class ProductDetailPresenter: ProductDetailPresenterProtocol {
    // MARK: - VIPER -
    private weak var view : ProductDetailViewControllerProtocol?
    private var router    : ProductDetailRouterProtocol?
    private var interactor: ProductDetailInteractorProtocol?
    
    func setup(view: ProductDetailViewControllerProtocol, router: ProductDetailRouterProtocol, interactor: ProductDetailInteractorProtocol) {
        self.view       = view
        self.router     = router
        self.interactor = interactor
    }
    
    
    // MARK: - ProductDetailPresenterProtocol -
    func readyToDisplay() {
        guard let product = interactor?.product else {
            return
        }
        
        setupViewForUserRole(interactor?.userRole)
        view?.displayProduct(product)
    }
    
    func setupViewForUserRole(_ userRold: UserRole?) {
        guard let userRold = userRold else {
            view?.isEditButtonEnabled = false
            return
        }
        
        switch userRold {
        case .user:
            view?.isEditButtonEnabled = false
        case .admin:
            view?.isEditButtonEnabled = true
        }
    }
    
    func editButtonWasTapped() {
        guard let userRole = interactor?.userRole, userRole == .admin, let productId = interactor?.product.id else {
            return
        }
        router?.showEditView(withProductId: productId)
    }
    
    func buyButtonWasTapped(withCount count: Int) {
        guard let _ = interactor?.userRole else {
            router?.showLoginView()
            return
        }
        interactor?.buy(count)
    }
    
    func productWasAddedToBasket() {
        router?.showProductWasAddedToBasketAllert()
    }
}
