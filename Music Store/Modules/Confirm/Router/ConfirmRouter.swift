//
//  ConfirmRouter.swift
//  Music Store
//
//  Created by Marty on 09/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class ConfirmRouter: ConfirmRouterProtocol {
    // VIPER
    private weak var interactor: ConfirmInteractorProtocol?
    
    private weak var navigation: NavigationViewController?
    
    init(interactor: ConfirmInteractorProtocol, navigationController: NavigationViewController) {
        self.interactor  = interactor
        self.navigation = navigationController
    }
    
    static func assemble(embadedIn navigationController: NavigationViewController) -> ConfirmInteractor {
        let interactor = ConfirmInteractor()
        let router = ConfirmRouter(interactor: interactor, navigationController: navigationController)
        interactor.setup(router: router)
        
        return interactor
    }
    
    // MARK: - ConfirmRouterProtocol -
    
    func confirmationSucceed() {
        navigation?.isNavigationBarHidden = false
        navigation?.destroyConfirmModule()
    }
    
    func confirmationFailured(_ response: String) {
        navigation?.showAlert(title: "Confirmation failured", message: response)
    }
}
