//
//  ConfirmUserResponseData.swift
//  Music Store
//
//  Created by Marty on 09/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

struct ConfirmUserResponseData: Encodable, Decodable {
    let accessToken: String
    let refreshToken: String
    let userRole: UserRole
}
