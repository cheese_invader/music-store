//
//  ConfirmRequest.swift
//  Music Store
//
//  Created by Marty on 09/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class ConfirmRequest {
    private var task: URLSessionDataTask?
    var completionSuccessHandler: ((ConfirmUserResponseData) -> Void)?
    var completionFailureHandler: ((String) -> Void)?
    
    deinit {
        task?.cancel()
    }
    
    private func composeWithRequest(_ request: URLRequest) {
        task = URLSession.shared.dataTask(with: request, completionHandler: { [weak self] (data, response, error) in
            guard let self = self else {
                return
            }
                        
            if let error = error {
                self.completionFailureHandler?(error.localizedDescription)
                return
            }
            
            guard let urlResponse = response as? HTTPURLResponse else {
                self.completionFailureHandler?("Wrong response format")
                return
            }
            
            guard urlResponse.statusCode == 200, let data = data else {
                self.completionFailureHandler?("Not ok status")
                return
            }
            
            guard let tokens = try? JSONDecoder().decode(ConfirmUserResponseData.self, from: data) else {
                self.completionFailureHandler?("Invalid tokens format")
                return
            }
            self.completionSuccessHandler?(tokens)
            
            return
        })
    }
    
    func perform(userData: ConfirmUserRequestData) {
        task?.cancel()
    
        
        let fuckingUrlString = Config.shared.serverURL.absoluteString + "/" + Config.shared.api_Confirm + "?email=\(userData.email)&loginToken=\(userData.loginToken)"
        
        let fuckingUrl = URL(string: fuckingUrlString)!
        
        var request = URLRequest(url: fuckingUrl)
        
        request.httpMethod = "POST"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        composeWithRequest(request)
        
        task?.resume()
    }
}

