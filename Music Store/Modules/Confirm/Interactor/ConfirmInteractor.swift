//
//  ConfirmInteractor.swift
//  Music Store
//
//  Created by Marty on 09/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class ConfirmInteractor: ConfirmInteractorProtocol {
    // VIPER
    private var router: ConfirmRouterProtocol?
    
    
    private var confirmRequest = ConfirmRequest()
    
    init() {
        initHandlers()
    }
    
    func setup(router: ConfirmRouterProtocol) {
        self.router = router
    }
    
    func confirm(email: String, loginToken: String) {
        confirmRequest.perform(userData: ConfirmUserRequestData(email: email, loginToken: loginToken))
    }
    
    private func initHandlers() {
        confirmRequest.completionSuccessHandler = { [weak self] (userInfo) in
            guard let self = self else {
                return
            }
            
            self.setupTokens(userInfo)
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.router?.confirmationSucceed()
            }
        }
        
        confirmRequest.completionFailureHandler = { [weak self] (confirmError) in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.router?.confirmationFailured(confirmError)
            }
        }
    }
    
    private func setupTokens(_ userInfo: ConfirmUserResponseData) {
        UserInfo.shared.accessToken  = userInfo.accessToken
        UserInfo.shared.refreshToken = userInfo.refreshToken
        UserInfo.shared.userRole     = userInfo.userRole
    }
}
