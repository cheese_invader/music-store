//
//  BalanceUpdater.swift
//  Music Store
//
//  Created by Marty on 17/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation


class BalanceUpdater {
    private var task: URLSessionDataTask?
    var completionSuccessHandler: (() -> Void)?
    var completionFailureHandler: ((String) -> Void)?
    
    deinit {
        task?.cancel()
    }
    
    private func composeWithRequest(_ request: URLRequest) {
        task = URLSession.shared.dataTask(with: request, completionHandler: { [weak self] (data, response, error) in
            guard let self = self else {
                return
            }
            
            if let error = error {
                self.completionFailureHandler?(error.localizedDescription)
                return
            }
            
            guard let urlResponse = response as? HTTPURLResponse else {
                self.completionFailureHandler?("Product updating failure")
                return
            }
            
            switch urlResponse.statusCode {
            case 200:
                self.completionSuccessHandler?()
            default:
                self.completionFailureHandler?("Not OK status")
            }
            return
        })
    }
    
    func perform(transactionInfo: BalanceUpdaterInfo) {
        task?.cancel()
        
        var requestUrl = Config.shared.serverURL
        
        switch transactionInfo.mode {
        case .transfer:
            requestUrl.appendPathComponent(Config.shared.api_TransactionTransfer)
        case .topUp:
            requestUrl.appendPathComponent(Config.shared.api_TransactionTopUp)
        }
        
        var request = URLRequest(url: requestUrl)
        
        let jsonEncoder = JSONEncoder()
        var jsonData: Data!
        
        switch transactionInfo.mode {
        case .transfer:
            guard let data = try? jsonEncoder.encode(TransactionScheme(info: transactionInfo)) else {
                completionFailureHandler?("Json compose error")
                return
            }
            jsonData = data
        case .topUp:
            guard let data = try? jsonEncoder.encode(TopUpScheme(info: transactionInfo)) else {
                completionFailureHandler?("Json compose error")
                return
            }
            jsonData = data
        }
        
        request.httpBody = jsonData
        
        request.httpMethod = "POST"
        request.setValue("Bearer \(UserInfo.shared.accessToken!)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        composeWithRequest(request)
        
        task?.resume()
    }
}
