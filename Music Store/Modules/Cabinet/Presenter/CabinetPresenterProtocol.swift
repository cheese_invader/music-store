//
//  CabinetPresenterProtocol.swift
//  Music Store
//
//  Created by Marty on 17/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

protocol CabinetPresenterProtocol: class {
    func topUpBalanceButtonWasTapped()
    func transactionSendButtonWasTapped()
    func openBasketButtonWasTapped()
    func openHistoryButtonWasTapped()
    func logOutButtonWasTapped()
    
    func orderBalance()
    
    func topUpSucceed()
    func topUpFailured()
    
    func transferSucceed()
    func transferFailured()
    
    func gotBalance(_ sum: Decimal)
    func balanceRequestFailure()
}
