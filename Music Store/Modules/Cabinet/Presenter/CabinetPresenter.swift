//
//  CabinetPresenter.swift
//  Music Store
//
//  Created by Marty on 17/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class CabinetPresenter: CabinetPresenterProtocol {
    // MARK: - VIPER -
    private weak var view : CabinetViewControllerProtocol?
    private var router    : CabinetRouterProtocol?
    private var interactor: CabinetInteractorProtocol?
    
    func setup(view: CabinetViewControllerProtocol, router: CabinetRouterProtocol, interactor: CabinetInteractorProtocol) {
        self.view       = view
        self.router     = router
        self.interactor = interactor
    }
    
    
    private var isTransactionsEnable: Bool {
        get {
            return view?.isTopUpEnabled ?? false
        }
        set {
            view?.isTopUpEnabled    = newValue
            view?.isTransferEnabled = newValue
        }
    }
    
    private func clearInputs() {
        view?.transferEmail   = nil
        view?.transferSum     = nil
        view?.topUpBalanceSum = nil
    }
    
    // MARK: - CabinetPresenterProtocol -
    func topUpBalanceButtonWasTapped() {
        guard let balance = view?.topUpBalanceSum else {
            return
        }
        
        isTransactionsEnable = false
        interactor?.topUpBalanceWith(balance)
    }
    
    func transactionSendButtonWasTapped() {
        guard let email = view?.transferEmail, let sum = view?.transferSum else {
            return
        }
        
        isTransactionsEnable = false
        interactor?.transferMonyTo(email, sum: sum)
    }
    
    func openBasketButtonWasTapped() {
        router?.openBasket()
    }
    
    func openHistoryButtonWasTapped() {
        router?.openHistory()
    }
    
    func logOutButtonWasTapped() {
        interactor?.logOut()
        router?.dismiss()
    }
    
    func orderBalance() {
        interactor?.orderBalance()
    }
    
    
    func topUpSucceed() {
        isTransactionsEnable = true
        view?.setBalanceUnknown()
        interactor?.orderBalance()
        router?.showAlert(title: "Top up succeed", message: "")
        clearInputs()
    }
    
    func topUpFailured() {
        isTransactionsEnable = true
        router?.showAlert(title: "Top up failured", message: "")
    }
    
    func transferSucceed() {
        isTransactionsEnable = true
        view?.setBalanceUnknown()
        interactor?.orderBalance()
        router?.showAlert(title: "Transfer succeed", message: "")
        clearInputs()
    }
    
    func transferFailured() {
        isTransactionsEnable = true
        router?.showAlert(title: "Transfer failured", message: "")
    }
    
    
    func gotBalance(_ sum: Decimal) {
        view?.setupBalance(sum)
    }
    
    func balanceRequestFailure() {
        router?.showAlert(title: "Got balance failure", message: "")
    }
}
