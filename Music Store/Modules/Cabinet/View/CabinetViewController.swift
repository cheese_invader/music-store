//
//  CabinetViewController.swift
//  Music Store
//
//  Created by Marty on 17/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

class CabinetViewController: UIViewController, CabinetViewControllerProtocol {
    // MARK: - VIPER -
    private var presenter: CabinetPresenterProtocol?
    
    func setupPresenter(_ presenter: CabinetPresenterProtocol) {
        self.presenter = presenter
    }
    
    
    // MARK: - Outlet -
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var topUpSumTextField: UITextField!
    @IBOutlet weak var transactionEmailTextField: UITextField!
    @IBOutlet weak var transactionSumTextField: UITextField!
    @IBOutlet weak var transactionSendButton: UIButton!
    @IBOutlet weak var topUpButton: UIButton!
    
    @IBOutlet weak var balanceActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var topUpActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var transferActivityIndicator: UIActivityIndicatorView!
    
    
    
    // MARK: - Action -
    @IBAction func TopUpBalanceButtonWasTapped(_ sender: UIButton) {
        presenter?.topUpBalanceButtonWasTapped()
    }
    
    @IBAction func transactionSendButtonWasTapped(_ sender: UIButton) {
        presenter?.transactionSendButtonWasTapped()
    }
    
    @IBAction func openBasketButtonWasTapped(_ sender: UIButton) {
        presenter?.openBasketButtonWasTapped()
    }
    
    @IBAction func openHistoryButtonWasTapped(_ sender: UIButton) {
        presenter?.openHistoryButtonWasTapped()
    }
    
    @IBAction func logOutButtonWasTapped(_ sender: UIButton) {
        presenter?.logOutButtonWasTapped()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupBackground()
        makePlaceholdersWhite()
        hideKeyboardWhenTappedAround()
        presenter?.orderBalance()
    }

    
    // MARK: - Helpers -
    private func setupBackground() {
        view.backgroundColor = UIColor(patternImage: UIImage(named: "Les Paul")!)
    }
    
    private func makePlaceholdersWhite() {
        topUpSumTextField.attributedPlaceholder = NSAttributedString(string: "sum", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        transactionEmailTextField.attributedPlaceholder = NSAttributedString(string: "email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        transactionSumTextField.attributedPlaceholder = NSAttributedString(string: "sum", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
    }
    
    
    // MARK: - CabinetViewControllerProtocol -
    var topUpBalanceSum: Decimal? {
        get {
            guard let rawSum = topUpSumTextField.text else {
                return nil
            }
            return Decimal(string: rawSum)
        }
        
        set {
            topUpSumTextField.text = newValue != nil ? "\(newValue!)" : ""
        }
    }
    
    var transferEmail: String? {
        get {
            return transactionEmailTextField.text
        }
        
        set {
            transactionEmailTextField.text = newValue ?? ""
        }
    }
    
    var transferSum: Decimal? {
        get {
            guard let rawSum = transactionSumTextField.text else {
                return nil
            }
            return Decimal(string: rawSum)
        }
        
        set {
            transactionSumTextField.text = newValue != nil ? "\(newValue!)" : ""
        }
    }
    
    var isTopUpEnabled: Bool {
        get {
            return topUpButton.isEnabled
        }
        
        set {
            if newValue {
                topUpActivityIndicator.stopAnimating()
            } else {
                topUpActivityIndicator.startAnimating()
            }
            topUpButton.isEnabled = newValue
        }
    }
    
    var isTransferEnabled: Bool {
        get {
            return transactionSendButton.isEnabled
        }
        
        set {
            if newValue {
                transferActivityIndicator.stopAnimating()
            } else {
                transferActivityIndicator.startAnimating()
            }
            transactionSendButton.isEnabled = newValue
        }
    }
    
    func setupBalance(_ balance: Decimal) {
        balanceActivityIndicator.stopAnimating()
        balanceLabel.text = "$ \(balance)"
        balanceLabel.isHidden = false
    }
    
    func setBalanceUnknown() {
        balanceLabel.isHidden = true
        balanceActivityIndicator.startAnimating()
    }
}
