//
//  CabinetViewControllerProtocol.swift
//  Music Store
//
//  Created by Marty on 17/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

protocol CabinetViewControllerProtocol: class {
    var topUpBalanceSum: Decimal? { get set }
    var transferEmail  : String?  { get set }
    var transferSum    : Decimal? { get set }
    
    var isTopUpEnabled   : Bool { get set }
    var isTransferEnabled: Bool { get set }
    
    func setupBalance(_ balance: Decimal)
    func setBalanceUnknown()
    
}
