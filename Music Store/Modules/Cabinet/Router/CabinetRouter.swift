//
//  CabinetRouter.swift
//  Music Store
//
//  Created by Marty on 17/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit


private let storyboardName = "Cabinet"
private let storyboardID   = "CabinetStoryboard"


class CabinetRouter: CabinetRouterProtocol {
    // MARK: - VIPER -
    private weak var presenter:  CabinetPresenterProtocol?
    private weak var navigationController: NavigationViewController?
    
    init(presenter:  CabinetPresenterProtocol, navigationController: NavigationViewController) {
        self.navigationController = navigationController
        self.presenter            = presenter
    }
    
    static func assembleMudule(embededIn navigationController: NavigationViewController) -> CabinetViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let view = storyboard.instantiateViewController(withIdentifier: storyboardID) as! CabinetViewController
        
        let presenter = CabinetPresenter()
        let interactor = CabinetInteractor(presenter: presenter)
        let router = CabinetRouter(presenter: presenter, navigationController: navigationController)
        
        presenter.setup(view: view, router: router, interactor: interactor)
        view.setupPresenter(presenter)
        
        return view
    }
    
    
    // MARK: - CabinetRouterProtocol -
    func showAlert(title: String, message: String) {
        guard let navigationController = navigationController else {
            return
        }
        navigationController.showQuickAllert(title: title, message: message)
    }
    
    func dismiss() {
        guard let navigationController = navigationController else {
            return
        }
        navigationController.popViewController(animated: true)
    }
    
    func openBasket() {
        guard let navigationController = navigationController else {
            return
        }
        let basketVC = BasketRouter.assembleMudule(embededIn: navigationController)
        navigationController.pushViewController(basketVC, animated: true)
    }
    
    func openHistory() {
        guard let navigationController = navigationController else {
            return
        }
        let historyVC = HistoryRouter.assembleMudule(embededIn: navigationController)
        navigationController.pushViewController(historyVC, animated: true)
    }
}
