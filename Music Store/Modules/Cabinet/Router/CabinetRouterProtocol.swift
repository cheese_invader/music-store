//
//  CabinetRouterProtocol.swift
//  Music Store
//
//  Created by Marty on 17/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol CabinetRouterProtocol: class {
    func showAlert(title: String, message: String)
    func dismiss()
    func openBasket()
    func openHistory()
}
