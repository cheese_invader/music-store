//
//  TopUpScheme.swift
//  Music Store
//
//  Created by Marty on 17/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class TopUpScheme: Encodable, Decodable {
    var sum: Decimal
    
    init(info: BalanceUpdaterInfo) {
        self.sum   = info.sum
    }
}
