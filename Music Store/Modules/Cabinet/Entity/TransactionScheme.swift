//
//  TransactionScheme.swift
//  Music Store
//
//  Created by Marty on 17/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class TransactionScheme: Encodable, Decodable {
    var email: String
    var sum: Decimal
    
    init(info: BalanceUpdaterInfo) {
        self.email = info.email ?? ""
        self.sum   = info.sum
    }
}
