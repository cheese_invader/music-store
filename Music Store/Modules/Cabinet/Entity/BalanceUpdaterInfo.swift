//
//  BalanceUpdaterInfo.swift
//  Music Store
//
//  Created by Marty on 17/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

struct BalanceUpdaterInfo {
    var sum: Decimal
    var email: String?
    
    var mode: BalanceUpdateMode {
        return email == nil ? .topUp : .transfer
    }
}
