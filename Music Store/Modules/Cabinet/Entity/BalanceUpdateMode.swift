//
//  BalanceUpdateMode.swift
//  Music Store
//
//  Created by Marty on 17/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

enum BalanceUpdateMode {
    case topUp
    case transfer
}
