//
//  CabinetInteractorProtocol.swift
//  Music Store
//
//  Created by Marty on 17/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

protocol CabinetInteractorProtocol: class {
    func topUpBalanceWith(_ sum: Decimal)
    func transferMonyTo(_ email: String, sum: Decimal)
    func orderBalance()
    func logOut()
}
