//
//  CabinetInteractor.swift
//  Music Store
//
//  Created by Marty on 17/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class CabinetInteractor: CabinetInteractorProtocol {
    // MARK: - VIPER -
    private weak var presenter: CabinetPresenterProtocol?
    
    init(presenter: CabinetPresenterProtocol) {
        self.presenter = presenter
        setupGetBalanceHandlers()
    }
    
    
    let balanceUpdater = BalanceUpdater()
    let getBalanceRequest = GetBalanceRequest()
    
    // MARK: - Helpers -
    private func setupGetBalanceHandlers() {
        getBalanceRequest.completionSuccessHandler = { [weak self] (balance) in
            DispatchQueue.main.async {
                guard let self = self else {
                    return
                }
                self.presenter?.gotBalance(balance)
            }
        }
        
        getBalanceRequest.completionFailureHandler = { [weak self] (_) in
            DispatchQueue.main.async {
                guard let self = self else {
                    return
                }
                self.presenter?.balanceRequestFailure()
            }
        }
    }
    
    private func setupTopUpHandlers() {
        balanceUpdater.completionSuccessHandler = { [weak self] in
            DispatchQueue.main.async {
                guard let self = self else {
                    return
                }
                self.presenter?.topUpSucceed()
            }
        }
        
        balanceUpdater.completionFailureHandler = { [weak self] (_) in
            DispatchQueue.main.async {
                guard let self = self else {
                    return
                }
                self.presenter?.topUpFailured()
            }
        }
    }
    
    private func setupTransferHandlers() {
        balanceUpdater.completionSuccessHandler = { [weak self] in
            DispatchQueue.main.async {
                guard let self = self else {
                    return
                }
                self.presenter?.transferSucceed()
            }
        }
        
        balanceUpdater.completionFailureHandler = { [weak self] (_) in
            DispatchQueue.main.async {
                guard let self = self else {
                    return
                }
                self.presenter?.transferFailured()
            }
        }
    }
    
    
    // MARK: - CabinetInteractorProtocol -
    func topUpBalanceWith(_ sum: Decimal) {
        setupTopUpHandlers()
        
        let info = BalanceUpdaterInfo(sum: sum, email: nil)
        balanceUpdater.perform(transactionInfo: info)
    }
    
    func transferMonyTo(_ email: String, sum: Decimal) {
        setupTransferHandlers()
        let info = BalanceUpdaterInfo(sum: sum, email: email)
        balanceUpdater.perform(transactionInfo: info)
    }
    
    func orderBalance() {
        getBalanceRequest.perform()
    }
    
    func logOut() {
        UserInfo.shared.clear()
    }
}
