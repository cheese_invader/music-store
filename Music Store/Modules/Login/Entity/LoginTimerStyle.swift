//
//  LoginTimerStyle.swift
//  Music Store
//
//  Created by Marty on 01/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import CoreGraphics

private let colorMarty      : CGColor = #colorLiteral(red: 0.9994240403, green: 0.9855536819, blue: 0, alpha: 1)
private let colorDanger     : CGColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)

enum LoginTimerStyle {
    case usual
    case danger
    case timeOver
    
    var color: CGColor {
        get {
            switch self {
            case .usual:
                return colorMarty
            case .danger, .timeOver:
                return colorDanger
            }
        }
    }
}
