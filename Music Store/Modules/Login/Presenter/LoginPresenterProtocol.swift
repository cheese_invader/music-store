//
//  LoginPresenterProtocol.swift
//  Music Store
//
//  Created by Marty on 01/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol LoginPresenterProtocol: class {
    // -> Interactor
    func loginRequestSuccessfullySent()
    func loginRequestFailured(message: String)
    func setTimerStyle(_ style: LoginTimerStyle)
    func updateTimerValue(minutes: Int, seconds: Int)
    
    // -> View
    func sendLoginRequest(_ email: String)
    func sendLoginRequestAgain(_ email: String)
    func changeMail()
    func openMailApp()
}
