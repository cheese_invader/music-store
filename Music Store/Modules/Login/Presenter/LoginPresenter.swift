//
//  LoginPresenter.swift
//  Music Store
//
//  Created by Marty on 01/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//


private let timerTimeOut = 1
private let duration = 0.8


class LoginPresenter: LoginPresenterProtocol {
    // MARK: - VIPER -
    private weak var view : LoginViewControllerProtocol?
    private var router    : LoginRouterProtocol?
    private var interactor: LoginInteractorProtocol?
    
    func setup(view: LoginViewControllerProtocol, router: LoginRouterProtocol, interactor:LoginInteractorProtocol) {
        self.view       = view
        self.router     = router
        self.interactor = interactor
    }
    
    
    func loginRequestSuccessfullySent() {
        view?.isTextFieldAndSendButtonEnabled = false
        view?.isLoginActivityIndicatorShown = false
        view?.moveEmailTextField(-100, duration: duration, delay: 0)
        view?.setTimerHidden(false, duration: duration, delay: 1)
        view?.moveTimerLabel(-100, duration: duration, delay: 1)
        view?.changeButtonFromSendToMail(duration: duration, delay: 2)
    }
    
    func loginRequestFailured(message: String) {
        view?.isTextFieldAndSendButtonEnabled = true
        view?.isLoginActivityIndicatorShown = false
        view?.showLog(message)
    }
    
    func setTimerStyle(_ style: LoginTimerStyle) {
        view?.setTimerStyle(style)
        if style == .timeOver {
            view?.changeButtonFromMailToExpired(duration: duration, delay: 0)
            view?.setTimeValue("Expired")
        }
    }
    
    func updateTimerValue(minutes: Int, seconds: Int) {
        view?.setTimeValue("\(minutes):\(seconds)")
    }
    
    func sendLoginRequest(_ email: String) {
        guard !email.isEmpty else {
            view?.shakeEmailField()
            return
        }
        view?.hideLogger()
        view?.isLoginActivityIndicatorShown = true
        view?.isTextFieldAndSendButtonEnabled = false
        interactor?.sendRequest(email)
    }
    
    func sendLoginRequestAgain(_ email: String) {
        guard !email.isEmpty else {
            view?.shakeEmailField()
            return
        }
        view?.hideLogger()
        view?.isLoginActivityIndicatorShown = true
        view?.changeButtonFromExpiredToMail(duration: duration, delay: 0)
    }
    
    func changeMail() {
        view?.hideLogger()
        view?.isTextFieldAndSendButtonEnabled = true
        view?.setTimerHidden(true, duration: duration, delay: 0)
        view?.moveTimerLabel(100, duration: duration, delay: 0)
        view?.moveEmailTextField(100, duration: duration, delay: 0.5)
        view?.changeButtonFromExpiredToSend(duration: duration, delay: 1)
    }
    
    func openMailApp() {
        router?.openMailApp()
    }
}
