//
//  LoginViewControllerProtocol.swift
//  Music Store
//
//  Created by Marty on 01/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import CoreGraphics

protocol LoginViewControllerProtocol: class {
    // Sending locker
    var isTextFieldAndSendButtonEnabled: Bool { get set }
    
    var isLoginActivityIndicatorShown: Bool { get set }
    
    func moveEmailTextField(_ value: CGFloat, duration: Double, delay: Double)
    func moveTimerLabel(_ value: CGFloat, duration: Double, delay: Double)
    
    func setTimerHidden(_ isHidden: Bool, duration: Double, delay: Double)
    func setTimerStyle(_ style: LoginTimerStyle)
    func setTimeValue(_ value: String)
    
    func shakeEmailField()
    
    func changeButtonFromSendToMail   (duration: Double, delay: Double)
    func changeButtonFromMailToExpired(duration: Double, delay: Double)
    func changeButtonFromExpiredToMail(duration: Double, delay: Double)
    func changeButtonFromExpiredToSend(duration: Double, delay: Double)
        
    func showLog(_ log: String)
    func hideLogger()
}
