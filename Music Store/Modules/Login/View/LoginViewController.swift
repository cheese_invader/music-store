//
//  LoginViewController.swift
//  Music Store
//
//  Created by Marty on 01/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit


private let sendButtonLabel = "Send"

private let lineOffsetSide  : CGFloat = 50
private let lineOffsetBottom: CGFloat = 15
private let lineWidth       : CGFloat = 2
private let colorMarty      : CGColor = #colorLiteral(red: 0.9994240403, green: 0.9855536819, blue: 0, alpha: 1)
private let colorDanger     : CGColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)

private let damping       : CGFloat = 0.5
private let springVelocity: CGFloat = 0.5


class LoginViewController: UIViewController, LoginViewControllerProtocol {
    // MARK: - VIPER -
    private var presenter: LoginPresenterProtocol?
    
    func setupPresenter(_ presenter: LoginPresenterProtocol) {
        self.presenter = presenter
    }
    
    
    @IBOutlet weak var emailTextField: ShakeTextField!
    @IBOutlet weak var timerLabel: UILabel!
    
    @IBOutlet weak var loginActivityIndicator: UIActivityIndicatorView!
    
    
    @IBOutlet weak var sendButton: RoundButton!
    @IBOutlet weak var openMailButton: RoundButton!
    @IBOutlet weak var sendAgainButton: RoundButton!
    @IBOutlet weak var changeMailButton: RoundButton!
    
    @IBOutlet weak var loggerLabel: UILabel!
    
    
    @IBOutlet weak var emailTextFieldCenterYConstraint: NSLayoutConstraint!
    @IBOutlet weak var timerLabelCenterYConstraint: NSLayoutConstraint!
    @IBOutlet weak var openMailButtonCenterYConstraint: NSLayoutConstraint!
    @IBOutlet weak var sendButtonCenterYConstraint: NSLayoutConstraint!
    
    @IBAction func emailTextFieldPrimaryActionTriggered(_ sender: ShakeTextField) {
        presenter?.sendLoginRequest(emailTextField.text ?? "")
    }
    
    @IBAction func sendButtonWasTapped(_ sender: RoundButton) {
        presenter?.sendLoginRequest(emailTextField.text ?? "")
    }
    
    @IBAction func openMailButtonWasTapped(_ sender: RoundButton) {
        presenter?.openMailApp()
    }
    
    @IBAction func changeMailButtonWasTapped(_ sender: RoundButton) {
        presenter?.changeMail()
    }
    
    @IBAction func sendAgainButtonWasTapped(_ sender: RoundButton) {
        presenter?.sendLoginRequestAgain(emailTextField.text ?? "")
    }
    
    
    var sendButtonToOpenMailWidthAnchor: NSLayoutConstraint!
    var sendButtonToExpiredWidthAnchor : NSLayoutConstraint!
    var openMailWidthAnchor            : NSLayoutConstraint!
    var sendAgainButtonWidthAnchor     : NSLayoutConstraint!
    var changeMailButtonWidthAnchor    : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardWhenTappedAround()
        makeEmailTextFieldPlaceholderWhite()
        drawLineInCenter()
        setAnchors()
    }
    
    private func drawLineInCenter() {
        let path = UIBezierPath()
        
        path.move(to: CGPoint(x: lineOffsetSide, y: view.center.y + lineOffsetBottom))
        path.addLine(to: CGPoint(x: view.bounds.width - lineOffsetSide, y: view.center.y + lineOffsetBottom))
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = colorMarty
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = lineWidth
        
        view.layer.addSublayer(shapeLayer)
    }
    
    private func makeEmailTextFieldPlaceholderWhite() {
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
    }
    
    private func setAnchors() {
        let expiredButtonsWidth = sendAgainButton.frame.maxX - changeMailButton.frame.minX
        
        sendButtonToOpenMailWidthAnchor = sendButton.widthAnchor.constraint(equalToConstant: openMailButton.bounds.width)
        sendButtonToExpiredWidthAnchor  = sendButton.widthAnchor.constraint(equalToConstant: expiredButtonsWidth)
        
        openMailWidthAnchor         = openMailButton  .widthAnchor.constraint(equalToConstant: expiredButtonsWidth)
        sendAgainButtonWidthAnchor  = sendAgainButton .widthAnchor.constraint(equalToConstant: expiredButtonsWidth)
        changeMailButtonWidthAnchor = changeMailButton.widthAnchor.constraint(equalToConstant: expiredButtonsWidth)
        
        sendAgainButtonWidthAnchor .isActive = true
        changeMailButtonWidthAnchor.isActive = true
    }
    
    // MARK: - LoginViewControllerProtocol -
    
    var isTextFieldAndSendButtonEnabled: Bool {
        get {
            return emailTextField.isEnabled
        }
        set {
            emailTextField.isEnabled = newValue
            sendButton    .isEnabled = newValue
        }
    }
    
    var isLoginActivityIndicatorShown: Bool {
        get {
            return loginActivityIndicator.isAnimating
        }
        set {
            if newValue {
                loginActivityIndicator.startAnimating()
            } else {
                loginActivityIndicator.stopAnimating()
            }
        }
    }
    
    func moveEmailTextField(_ value: CGFloat, duration: Double, delay: Double) {
        emailTextFieldCenterYConstraint.constant += value
        layoutIfNeeded(duration: duration, delay: delay)
    }
    
    func moveTimerLabel(_ value: CGFloat, duration: Double, delay: Double) {
        timerLabelCenterYConstraint.constant += value
        layoutIfNeeded(duration: duration, delay: delay)
    }
    
    func setTimerHidden(_ isHidden: Bool, duration: Double, delay: Double) {
        timerLabel.alpha = 0
        timerLabel.isHidden = isHidden
        
        UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping: damping, initialSpringVelocity: springVelocity, options: .curveLinear, animations: { [weak self] in
            guard let self = self else {
                return
            }
            
            self.timerLabel.alpha = 1
            
            self.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    func setTimerStyle(_ style: LoginTimerStyle) {
        timerLabel.textColor = UIColor(cgColor: style.color)
    }
    
    func setTimeValue(_ value: String) {
        timerLabel.text = value
    }
    
    func shakeEmailField() {
        emailTextField.shake()
    }
    
    
    // MARK: - Button transitions -
    func changeButtonFromSendToMail(duration: Double, delay: Double) {
        sendButtonToOpenMailWidthAnchor.isActive = true
        sendButton.isEnabled = false
        
        UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveLinear, animations: { [weak self] in
            guard let self = self else {
                return
            }
            
            self.view.layoutIfNeeded()
            }, completion:{ [weak self] (_) in
                guard let self = self else {
                    return
                }
                
                self.openMailButton.isHidden = false
                self.sendButton    .isHidden = true
                self.openMailButton.isEnabled = true
                
                self.sendButton.setTitle(sendButtonLabel, for: .normal)
                
                self.sendButtonToOpenMailWidthAnchor.isActive = false
        })
    }
    
    func changeButtonFromMailToExpired(duration: Double, delay: Double) {
        let animationProportion = 7.0
        openMailButton.isEnabled = false
        openMailWidthAnchor.isActive = true
        
        UIView.animate(withDuration: duration / animationProportion, delay: delay, options: .curveLinear, animations: { [weak self] in
            guard let self = self else {
                return
            }
            
            self.view.layoutIfNeeded()
            }, completion: { [weak self] (_) in
                guard let self = self else {
                    return
                }
                
                self.openMailButton  .isHidden = true
                self.sendAgainButton .isHidden = false
                self.changeMailButton.isHidden = false
                
                self.sendAgainButtonWidthAnchor .isActive = false
                self.changeMailButtonWidthAnchor.isActive = false
                
                UIView.animate(withDuration: (duration / animationProportion) * (animationProportion - 1), delay: 0, usingSpringWithDamping: damping, initialSpringVelocity: springVelocity, options: .curveLinear, animations: { [weak self] in
                    guard let self = self else {
                        return
                    }
                    
                    self.view.layoutIfNeeded()
                    }, completion:{ [weak self] (_) in
                        guard let self = self else {
                            return
                        }
                        self.openMailWidthAnchor.isActive = false
                        
                        self.sendAgainButton .isEnabled = true
                        self.changeMailButton.isEnabled = true
                })
        })
    }
    
    func changeButtonFromExpiredToMail(duration: Double, delay: Double) {
        let animationProportion = 7.0
        changeMailButton.isEnabled = false
        sendAgainButton .isEnabled = false
        
        changeMailButtonWidthAnchor.isActive = true
        sendAgainButtonWidthAnchor .isActive = true
        openMailWidthAnchor        .isActive = true
        
        UIView.animate(withDuration: duration / animationProportion, delay: delay, options: .curveLinear, animations: { [weak self] in
            guard let self = self else {
                return
            }
            
            self.view.layoutIfNeeded()
            }, completion: { [weak self] (_) in
                guard let self = self else {
                    return
                }
                
                self.openMailButton  .isHidden = false
                self.sendAgainButton .isHidden = true
                self.changeMailButton.isHidden = true
                
                self.openMailWidthAnchor.isActive = false
                
                UIView.animate(withDuration: (duration / animationProportion) * (animationProportion - 1), delay: 0, usingSpringWithDamping: damping, initialSpringVelocity: springVelocity, options: .curveLinear, animations: { [weak self] in
                    guard let self = self else {
                        return
                    }
                    
                    self.view.layoutIfNeeded()
                    }, completion:{ [weak self] (_) in
                        guard let self = self else {
                            return
                        }
                        
                        self.openMailButton.isEnabled = true
                })
        })
    }
    
    func changeButtonFromExpiredToSend(duration: Double, delay: Double) {
        let animationProportion = 7.0
        changeMailButton.isEnabled = false
        sendAgainButton .isEnabled = false
        
        changeMailButtonWidthAnchor   .isActive = true
        sendAgainButtonWidthAnchor    .isActive = true
        sendButtonToExpiredWidthAnchor.isActive = true
        
        UIView.animate(withDuration: duration / animationProportion, delay: delay, options: .curveLinear, animations: { [weak self] in
            guard let self = self else {
                return
            }
            
            self.view.layoutIfNeeded()
            }, completion: { [weak self] (_) in
                guard let self = self else {
                    return
                }
                
                self.sendButton      .isHidden = false
                self.sendAgainButton .isHidden = true
                self.changeMailButton.isHidden = true
                
                self.sendButtonToExpiredWidthAnchor.isActive = false
                
                UIView.animate(withDuration: (duration / animationProportion) * (animationProportion - 1), delay: 0, usingSpringWithDamping: damping, initialSpringVelocity: springVelocity, options: .curveLinear, animations: { [weak self] in
                    guard let self = self else {
                        return
                    }
                    
                    self.view.layoutIfNeeded()
                    }, completion:{ [weak self] (_) in
                        guard let self = self else {
                            return
                        }
                        
                        self.sendButton.isEnabled = true
                })
        })
    }
    
    func showLog(_ log: String) {
        loggerLabel.text = log
        loggerLabel.isHidden = false
    }
    
    func hideLogger() {
        loggerLabel.isHidden = true
    }
    
    
    
    func layoutIfNeeded(duration: Double, delay: Double) {
        UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping: damping, initialSpringVelocity: springVelocity, options: .curveLinear, animations: { [weak self] in
            guard let self = self else {
                return
            }
            
            self.view.layoutIfNeeded()
            }, completion: nil)
    }
}
