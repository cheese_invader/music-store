//
//  LoginInteractorProtocol.swift
//  Music Store
//
//  Created by Marty on 01/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol LoginInteractorProtocol: class {
    func sendRequest(_ email: String)
}
