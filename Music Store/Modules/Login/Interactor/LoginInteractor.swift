//
//  LoginInteractor.swift
//  Music Store
//
//  Created by Marty on 01/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//


import Foundation

private let secondsInMinute   = 60
private let dangerSeconds     = 30
private let timerUpdateInterval = 0.5


class LoginInteractor: LoginInteractorProtocol {
    // MARK: - VIPER -
    private weak var presenter: LoginPresenterProtocol?
    
    init(presenter: LoginPresenterProtocol) {
        self.presenter = presenter
        setHandlers()
    }
    
    
    private var timer = Timer()
    private var deadline: Date!
    private var isDanger = false
    private let loginRequest = LoginRequest()
    
    
    // MARK: - Timer -
    
    @objc private func ticTac() {
        let difference = Int(deadline.timeIntervalSinceNow)
        
        if difference < dangerSeconds && !isDanger {
            presenter?.setTimerStyle(.danger)
            isDanger = true
        }
        if difference <= 0 {
            stopTimer()
            presenter?.setTimerStyle(.timeOver)
            return
        }
        
        presenter?.updateTimerValue(minutes: difference / secondsInMinute, seconds: difference % secondsInMinute)
    }
    
    private func stopTimer() {
        timer.invalidate()
    }
    
    private func setTimerToValue(minutes: Int) {
        deadline = Calendar.current.date(byAdding: .minute, value: minutes, to: Date())
        timer = Timer.scheduledTimer(timeInterval: timerUpdateInterval, target: self, selector: #selector(ticTac), userInfo: nil, repeats: true)
    }
    
    
    private func setHandlers() {
        loginRequest.completionSuccessHandler = { [weak self] in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.setTimerToValue(minutes: 5)
                self.presenter?.loginRequestSuccessfullySent()
            }
        }
        
        loginRequest.completionFailureHandler = { [weak self] (response) in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.presenter?.loginRequestFailured(message: response)
            }
        }
    }
    
    
    // MARK: - LoginInteractorProtocol -
    
    func sendRequest(_ email: String) {
        UserInfo.shared.email = email
        loginRequest.perform(email: email)
    }
}
