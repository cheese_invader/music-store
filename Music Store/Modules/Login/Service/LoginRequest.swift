//
//  LoginRequest.swift
//  Music Store
//
//  Created by Marty on 01/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class LoginRequest {
    private var task: URLSessionDataTask?
    var completionSuccessHandler: (() -> Void)?
    var completionFailureHandler: ((String) -> Void)?
    
    deinit {
        task?.cancel()
    }
    
    private func composeWithRequest(_ request: URLRequest) {
        task = URLSession.shared.dataTask(with: request, completionHandler: { [weak self] (data, response, error) in
            guard let self = self else {
                return
            }
            
            if let error = error {
                self.completionFailureHandler?(error.localizedDescription)
                return
            }
            
            guard let urlResponse = response as? HTTPURLResponse else {
                self.completionFailureHandler?("Login request failure")
                return
            }
            
            switch urlResponse.statusCode {
            case 200:
                self.completionSuccessHandler?()
            default:
                self.completionFailureHandler?("Request error")
            }
            return
        })
    }
    
    func perform(email: String) {
        task?.cancel()
        
        var request = URLRequest(url: Config.shared.serverURL.appendingPathComponent(Config.shared.api_Login))
        request.httpMethod = "POST"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")

        let jsonEmail = "\"\(email)\""
        request.httpBody = jsonEmail.data(using: .utf8)
        
        composeWithRequest(request)
        
        task?.resume()
    }
}
