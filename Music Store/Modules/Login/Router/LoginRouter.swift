//
//  LoginRouter.swift
//  Music Store
//
//  Created by Marty on 01/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit


private let storyboardName = "Login"
private let storyboardID   = "LoginStoryboard"


class LoginRouter: LoginRouterProtocol {
    // MARK: - VIPER -
    
    private weak var presenter:  LoginPresenterProtocol?
    private weak var navigationController: NavigationViewController?
    
    init(presenter:  LoginPresenterProtocol, navigationController: NavigationViewController) {
        self.navigationController = navigationController
        self.presenter            = presenter
        
        navigationController.setNavigationBarHidden(true, animated: false)
    }
    
    
    static func assembleMudule(embededIn navigationController: NavigationViewController) -> LoginViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let view = storyboard.instantiateViewController(withIdentifier: storyboardID) as! LoginViewController
        
        let presenter = LoginPresenter()
        let interactor = LoginInteractor(presenter: presenter)
        let router = LoginRouter(presenter: presenter, navigationController: navigationController)
        
        presenter.setup(view: view, router: router, interactor: interactor)
        view.setupPresenter(presenter)
        
        return view
    }
    
    func openMailApp() {
        let mailURL = URL(string: "message://")!
        
        if UIApplication.shared.canOpenURL(mailURL) {
            UIApplication.shared.open(mailURL, options: [:], completionHandler: nil)
        }
    }
}
