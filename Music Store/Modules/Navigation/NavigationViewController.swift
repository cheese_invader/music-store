//
//  NavigationViewController.swift
//  Music Store
//
//  Created by Marty on 20/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit
import Foundation

private let storyboardName = "Navigation"
private let storyboardID   = "NavigationStoryboard"


class NavigationViewController: UINavigationController {
    private var confirmModule: ConfirmInteractorProtocol?
    
    static let shared = UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: storyboardID) as! NavigationViewController
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        var request = URLRequest(url: URL(string: "http://172.20.10.6:1024/api/shop-window/admin-rights")!)
//        request.setValue("Bearer \(UserInfo.shared.accessToken!)", forHTTPHeaderField: "Authorization")
//        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
//        request.httpMethod = "GET"
//        print(UserInfo.shared.accessToken!)
//        URLSession.shared.dataTask(with: request) { (data, response, error) in
//            if let error = error {
//                print("Error")
//                return
//            }
//
//            guard let urlResponse = response as? HTTPURLResponse else {
//                print("Fuck you")
//                return
//            }
//
//            print(urlResponse.statusCode)
//            guard urlResponse.statusCode == 200, let data = data else {
//                print("Fuck you man")
//                return
//            }
//            print(String(data: data, encoding: .utf8))
//        }.resume()
        
        
        let productList = ProductListRouter.assembleMudule(embededIn: self)
        self.pushViewController(productList, animated: true)
//
//
//        let loginVC = LoginRouter.assembleMudule(embededIn: self)
//        self.pushViewController(loginVC, animated: true)
    }
    
    func destroyConfirmModule() {
        confirmModule = nil
        setViewControllers([ProductListRouter.assembleMudule(embededIn: self)], animated: true)
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Got it", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func showQuickAllert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        
        let when = DispatchTime.now() + 2
        DispatchQueue.main.asyncAfter(deadline: when){
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    func appWasOpenedByURL(url: URL) {
        guard let email = UserInfo.shared.email, let loginToken = url["loginToken"] else {
            showAlert(title: "Invalid data", message: "")
            return
        }
        
        confirmModule = ConfirmRouter.assemble(embadedIn: self)
        confirmModule?.confirm(email: email, loginToken: loginToken)
    }
}
