//
//  ProductListPresenter.swift
//  Music Store
//
//  Created by Marty on 20/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class ProductListPresenter: ProductListPresenterProtocol {
    // MARK: - VIPER -
    private weak var view : ProductListCollectionViewControllerProtocol?
    private var router    : ProductListRouterProtocol?
    private var interactor: ProductListInteractorProtocol?
    
    func setup(view: ProductListCollectionViewControllerProtocol, router: ProductListRouterProtocol, interactor: ProductListInteractorProtocol) {
        
        self.view       = view
        self.router     = router
        self.interactor = interactor
    }
    
    
    // MARK: - ProductListPresenterProtocol -
    
    func gotProducts(_ products: [Product]) {
        view?.stopRefreshing()
        view?.displayProducts(products)
    }
    
    func gotErrorWhileLoadingProducts(_ error: String) {
        view?.stopRefreshing()
    }
    
    func refresh() {
        interactor?.loadProductIds()
    }
    
    func showDetailForProduct(_ product: Product) {
        router?.showDetailViewForProduct(product)
    }
    
    func cabinetButtonWasTapped() {
        if let _ = interactor?.userRold {
            router?.showCabinet()
        } else {
            router?.showLoginView()
        }
    }
    
    func addItemButtonWasTapped() {
        router?.showAddItemModule()
    }
}
