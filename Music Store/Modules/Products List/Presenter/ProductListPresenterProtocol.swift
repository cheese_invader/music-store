//
//  ProductListPresenterProtocol.swift
//  Music Store
//
//  Created by Marty on 20/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol ProductListPresenterProtocol: class {
    func gotProducts(_ products: [Product])
    func gotErrorWhileLoadingProducts(_ error: String)
    func refresh()
    
    func showDetailForProduct(_ product: Product)
    func cabinetButtonWasTapped()
    func addItemButtonWasTapped()
}

