//
//  ProductListCollectionViewCell.swift
//  Music Store
//
//  Created by Marty on 20/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

class ProductListCollectionViewCell: UICollectionViewCell {
    var product: Product!
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    
    func assembleWithProduct(_ product: Product) {
        self.product = product
        
        productImage.image    = product.image ?? UIImage(named: "Empty")
        titleLabel.text       = product.title
        descriptionLabel.text = product.description
        categoryLabel.text    = product.category
        countLabel.text       = "Count: \(product.count)"
    }
}
