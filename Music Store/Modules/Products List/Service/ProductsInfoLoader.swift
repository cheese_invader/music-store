//
//  ProductsInfoLoader.swift
//  Music Store
//
//  Created by Marty on 20/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation


class ProductsInfoLoader {
    private var task: URLSessionDataTask?
    var completionSuccessHandler: (([ProductSchema]) -> Void)?
    var completionFailureHandler: ((String) -> Void)?
    
    deinit {
        task?.cancel()
    }
    
    private func composeWithRequest(_ request: URLRequest) {
        task = URLSession.shared.dataTask(with: request, completionHandler: { [weak self] (data, response, error) in
            guard let self = self else {
                return
            }
            
            if let error = error {
                self.completionFailureHandler?(error.localizedDescription)
                return
            }
            
            guard let urlResponse = response as? HTTPURLResponse, let data = data else {
                self.completionFailureHandler?("Product loading failure")
                return
            }
                        
            switch urlResponse.statusCode {
            case 200:
                guard let productRequestSchema = try? JSONDecoder().decode(ProductRequestSchema.self, from: data) else {
                    self.completionFailureHandler?("Wrong response format")
                    return
                }
                self.completionSuccessHandler?(productRequestSchema.result)
            default:
                self.completionFailureHandler?("Not OK status")
            }
            return
        })
    }
    
    func performWithIds(_ ids: [Int]) {
        task?.cancel()
        
        var request = URLRequest(url: Config.shared.serverURL.appendingPathComponent(Config.shared.api_GetProductsByIds))
        request.httpMethod = "POST"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        let params = ids
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
        } catch {
            completionFailureHandler?("Request composition failured")
            return
        }
        
        composeWithRequest(request)
        
        task?.resume()
    }
}
