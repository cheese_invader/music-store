//
//  ProductListLoader.swift
//  Music Store
//
//  Created by Marty on 20/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//


import Foundation


class ProductListLoader {
    private var task: URLSessionDataTask?
    var completionSuccessHandler: (([Int]) -> Void)?
    var completionFailureHandler: ((String) -> Void)?
    
    deinit {
        task?.cancel()
    }
    
    private func composeWithRequest(_ request: URLRequest) {
        task = URLSession.shared.dataTask(with: request, completionHandler: { [weak self] (data, response, error) in
            guard let self = self else {
                return
            }
            
            if let error = error {
                self.completionFailureHandler?(error.localizedDescription)
                return
            }
            
            guard let urlResponse = response as? HTTPURLResponse, let data = data else {
                self.completionFailureHandler?("Product loading failure")
                return
            }
            
            switch urlResponse.statusCode {
            case 200:
                guard let productListIdsSchema = try? JSONDecoder().decode(ProductListIdsSchema.self, from: data) else {
                    self.completionFailureHandler?("Wrong response format")
                    return
                }
                self.completionSuccessHandler?(productListIdsSchema.result)
            default:
                self.completionFailureHandler?("Not OK status")
            }
            return
        })
    }
    
    func perform() {
        task?.cancel()
        
        var request = URLRequest(url: Config.shared.serverURL.appendingPathComponent(Config.shared.api_GetProductIds))
        request.httpMethod = "GET"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        composeWithRequest(request)
        
        task?.resume()
    }
}
