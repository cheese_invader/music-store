//
//  ProductListCollectionViewControllerProtocol.swift
//  Music Store
//
//  Created by Marty on 20/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol ProductListCollectionViewControllerProtocol: class {
    func displayProducts(_ products: [Product])
    func stopRefreshing()
}

