//
//  ProductListCollectionViewController.swift
//  Music Store
//
//  Created by Marty on 20/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit


private let reuseIdentifier = "ProductListCollectionViewCell"
private let cellInset : CGFloat = 10


class ProductListCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, ProductListCollectionViewControllerProtocol {
    // MARK: - VIPER -
    private var presenter: ProductListPresenterProtocol?
    
    func setupPresenter(_ presenter: ProductListPresenterProtocol) {
        self.presenter = presenter
    }
    
    private var products = [Product]()
    private let backgroundQueue = DispatchQueue(label: "com.marty.bgqueue", qos: .background)
    
    
    @IBOutlet weak var addItemButton: UIBarButtonItem!
    
    @IBOutlet weak var cabinetButton: UIBarButtonItem!
    
    @IBAction func addItemButtonWasTapped(_ sender: UIBarButtonItem) {
        presenter?.addItemButtonWasTapped()
    }
    
    @IBAction func cabinetButtonWasTapped(_ sender: UIBarButtonItem) {
        presenter?.cabinetButtonWasTapped()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBackgroundImage()
        registerNib()
        setupRefresher()
        
        guard let userRole = UserInfo.shared.userRole else {
            return
        }
        
        switch userRole {
        case .admin:
            addItemButton.isEnabled = true
        case .user:
            addItemButton.isEnabled = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        presenter?.refresh()
    }
    
    private func setBackgroundImage() {
        let bgImage = UIImage(named: "Les Paul")!
        let bgImageView = UIImageView(image: bgImage)
        bgImageView.contentMode = .scaleAspectFill
        collectionView.backgroundView = bgImageView
    }
    
    private func registerNib() {
        let nibCell = UINib(nibName: reuseIdentifier, bundle: nil)
        collectionView?.register(nibCell, forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    private func setupRefresher() {
        let refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(reload), for: .valueChanged)
        collectionView?.refreshControl = refresher
    }
    
    private func loadVisibleImages() {
        let visibleCells = self.collectionView.visibleCells
        
        backgroundQueue.async { [weak self] in
            guard let self = self else {
                return
            }
            
            for cell in visibleCells {
                let productCell = cell as! ProductListCollectionViewCell
                
                if productCell.product.image == nil {
                    let url = Config.shared.serverURL.appendingPathComponent(productCell.product.imagePath)
                    guard let data = try? Data(contentsOf: url) else {
                        continue
                    }
                    
                    productCell.product.image = UIImage(data: data)
                }
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.collectionView.reloadData()
            }
        }
    }
    
    @objc private func reload() {
        presenter?.refresh()
    }
    
    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ProductListCollectionViewCell
        
        cell.assembleWithProduct(products[indexPath.row])
        
        return cell
    }
    
    
    // MARK: - UICollectionViewDelegateFlowLayout -
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: CGFloat(cellInset), left: CGFloat(cellInset),
                            bottom: CGFloat(cellInset), right: CGFloat(cellInset))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var cellWidth : CGFloat!
        var cellHeight: CGFloat!
        
        if UIScreen.main.bounds.width > 500 {
            cellWidth = UIScreen.main.bounds.width / 4 - cellInset * 1.25
            cellHeight = cellWidth * 2
        } else {
            cellWidth = UIScreen.main.bounds.width / 2 - cellInset * 1.5
            cellHeight = cellWidth * 2
        }
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        loadVisibleImages()
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter?.showDetailForProduct(products[indexPath.row])
    }
    
    
    // MARK: - ProductListCollectionViewControllerProtocol -

    func displayProducts(_ products: [Product]) {
        self.products = products
        
        collectionView.reloadData()
        
        // This fu**ing main queue works strange. We are already in main queue. Why do I need call async method here???
        DispatchQueue.main.async {
            self.loadVisibleImages()
        }
    }
    
    func stopRefreshing() {
        collectionView?.refreshControl?.endRefreshing()
    }
}
