//
//  ProductListInteractor.swift
//  Music Store
//
//  Created by Marty on 20/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class ProductListInteractor: ProductListInteractorProtocol {
    // MARK: - VIPER -
    private weak var presenter: ProductListPresenterProtocol?
    
    init(presenter: ProductListPresenterProtocol) {
        self.presenter = presenter
        setupLoaderHandlers()
    }
    
    
    private var  productIdsLoader = ProductListLoader()
    private var productInfoLoader = ProductsInfoLoader()
    
    private var inProcess = false
    
    private func setupLoaderHandlers() {
        productIdsLoader.completionSuccessHandler = { [weak self] (ids) in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.productInfoLoader.performWithIds(ids)
            }
        }
        
        productIdsLoader.completionFailureHandler = { [weak self] (error) in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.inProcess = false
                self.presenter?.gotErrorWhileLoadingProducts(error)
            }
        }
        
        productInfoLoader.completionSuccessHandler = { [weak self] (products) in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                var handledProducts = [Product]()
                
                for prod in products {
                    handledProducts.append(Product(withProductSchema: prod))
                }
                
                self.inProcess = false
                
                self.presenter?.gotProducts(handledProducts)
            }
        }
        
        productInfoLoader.completionFailureHandler = { [weak self] (error) in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.inProcess = false
                self.presenter?.gotErrorWhileLoadingProducts(error)
            }
        }
    }
    
    
    // MARK: - ProductListInteractorProtocol -
    
    var userRold: UserRole? {
        get {
            return UserInfo.shared.userRole
        }
    }
    
    func loadProductIds() {
        if inProcess {
            return
        }
        inProcess = true
        productIdsLoader.perform()
    }
}
