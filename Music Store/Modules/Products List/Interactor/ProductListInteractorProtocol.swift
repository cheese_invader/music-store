//
//  ProductListInteractorProtocol.swift
//  Music Store
//
//  Created by Marty on 20/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol ProductListInteractorProtocol: class {
    var userRold: UserRole? { get }
    func loadProductIds()
}
