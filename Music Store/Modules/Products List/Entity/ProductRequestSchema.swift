//
//  ProductRequestSchema.swift
//  Music Store
//
//  Created by Marty on 20/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

struct ProductRequestSchema: Encodable, Decodable {
    var result: [ProductSchema]
    var message: String
}
