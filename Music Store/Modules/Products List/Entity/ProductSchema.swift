//
//  ProductSchema.swift
//  Music Store
//
//  Created by Marty on 20/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

struct ProductSchema: Encodable, Decodable {
    var id: Int
    
    var title      : String
    var description: String
    var category   : String
    var quantity   : Int
    var imagePath  : String
    var price      : Decimal
}
