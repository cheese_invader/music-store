//
//  ProductListIdsSchema.swift
//  Music Store
//
//  Created by Marty on 20/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

struct ProductListIdsSchema: Encodable, Decodable {
    var result: [Int]
    var message: String
}
