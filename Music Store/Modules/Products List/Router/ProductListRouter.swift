//
//  ProductListRouter.swift
//  Music Store
//
//  Created by Marty on 20/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit


private let storyboardName = "ProductList"
private let storyboardID   = "ProductListStoryboard"


class ProductListRouter: ProductListRouterProtocol {
    // MARK: - VIPER -
    
    private weak var presenter: ProductListPresenterProtocol?
    private weak var navigationController: NavigationViewController?
    
    init(presenter: ProductListPresenterProtocol, navigationController: NavigationViewController) {
        self.navigationController = navigationController
        self.presenter            = presenter
    }
    
    
    static func assembleMudule(embededIn navigationController: NavigationViewController) -> ProductListCollectionViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let view = storyboard.instantiateViewController(withIdentifier: storyboardID) as! ProductListCollectionViewController
        
        let presenter = ProductListPresenter()
        let interactor = ProductListInteractor(presenter: presenter)
        let router = ProductListRouter(presenter: presenter, navigationController: navigationController)
        
        presenter.setup(view: view, router: router, interactor: interactor)
        view.setupPresenter(presenter)
        
        return view
    }
    
    func showDetailViewForProduct(_ product: Product) {
        guard let navigationController = navigationController else {
            return
        }
        
        let detailView = ProductDetailRouter.assembleMudule(withProduct: product, embededIn: navigationController)
        navigationController.pushViewController(detailView, animated: true)
    }
    
    func showLoginView() {
        guard let navigationController = navigationController else {
            return
        }
        
        let loginView = LoginRouter.assembleMudule(embededIn: navigationController)
        navigationController.pushViewController(loginView, animated: true)
    }
    
    func showCabinet() {
        guard let navigationController = navigationController else {
            return
        }
        
        let cabinetView = CabinetRouter.assembleMudule(embededIn: navigationController)
        navigationController.pushViewController(cabinetView, animated: true)
    }
    
    func showAddItemModule() {
        guard let navigationController = navigationController else {
            return
        }
        
        let editView = ProductEditorRouter.assembleMudule(embededIn: navigationController, productId: "", mode: .add)
        navigationController.pushViewController(editView, animated: true)
    }
}
