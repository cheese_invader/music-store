//
//  ProductListRouterProtocol.swift
//  Music Store
//
//  Created by Marty on 20/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol ProductListRouterProtocol: class {
    func showDetailViewForProduct(_ product: Product)
    func showLoginView()
    func showCabinet()
    func showAddItemModule()
}
