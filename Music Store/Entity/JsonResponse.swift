//
//  JsonResponse.swift
//  Music Store
//
//  Created by Marty on 01/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

struct JsonResponse: Encodable, Decodable {
    let result: String?
    let message: String
}
