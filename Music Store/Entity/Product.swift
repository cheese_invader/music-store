//
//  Product.swift
//  Music Store
//
//  Created by Marty on 20/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit
import Foundation

class Product {
    var id          = ""
    var title       = ""
    var description = ""
    var category    = ""
    var count       = 0
    var imagePath   = ""
    var image       : UIImage?
    var price       : Decimal
    
    init(withProductSchema productSchema: ProductSchema) {
        self.id          = String(productSchema.id)
        self.title       = productSchema.title
        self.description = productSchema.description
        self.category    = productSchema.category
        self.count       = productSchema.quantity
        self.imagePath   = productSchema.imagePath
        self.price       = productSchema.price
    }
}
