//
//  UserInfo.swift
//  Music Store
//
//  Created by Marty on 09/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation


private let emailKey        = "ui_email"
private let accessTokenKey  = "ui_accessToken"
private let refreshTokenKey = "ui_refreshToken"
private let userRoleKey   = "ui_userRole"


class UserInfo {
    static let shared = UserInfo()
    
    private init() {}
    
    func clear() {
        email        = nil
        accessToken  = nil
        refreshToken = nil
        userRole     = nil
    }
    
    var isLogged: Bool {
        return accessToken != nil && refreshToken != nil
    }
    
    var email: String? {
        get {
            return UserDefaults.standard.string(forKey: emailKey)
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: emailKey)
        }
    }
    
    var accessToken: String? {
        get {
            return UserDefaults.standard.string(forKey: accessTokenKey)
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: accessTokenKey)
        }
    }
    
    var refreshToken: String? {
        get {
            return UserDefaults.standard.string(forKey: refreshTokenKey)
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: refreshTokenKey)
        }
    }
    
    var userRole: UserRole? {
        get {
            return UserRole(rawValue: UserDefaults.standard.string(forKey: userRoleKey) ?? "")
        }
        
        set {
            UserDefaults.standard.set(newValue?.rawValue ?? "", forKey: userRoleKey)
        }
    }
}
