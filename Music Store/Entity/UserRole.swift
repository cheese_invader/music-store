//
//  UserRole.swift
//  Music Store
//
//  Created by Marty on 09/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

enum UserRole: String, Encodable, Decodable {
    case admin = "Admin"
    case user = "User"
}
