//
//  Config.swift
//  Music Store
//
//  Created by Marty on 20/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class Config {
    static let shared = Config()
    
    private init() {}
    
    let serverURL = URL(string: "http://172.20.10.6:1024")!
    let api_GetProductIds       = "api/shop-window/products-ids"
    let api_GetProductsByIds    = "api/shop-window/products"
    let api_Login               = "api/account/auth"
    let api_Confirm             = "api/account/confirm"
    let api_UpdateProduct       = "api/shop-window/products/edit"
    let api_AddProduct          = "api/shop-window/products/add"
    let api_TransactionTopUp    = "api/bank/add"
    let api_TransactionTransfer = "api/bank/transfer"
    let api_GetBalance          = "api/bank/balance"
    let api_Buy                 = "api/products/buy"
    let api_History             = "api/products/history"
}
